local scene = {}

function scene:enter()
    -- limit the player movement speed
    self.playerMovementCooldownMax = 0.15 -- in seconds
    self.playerMovementCooldownRemaining = 0

    self.music = Music.ambient_starfield
    self.music:setPitch(140.0/160.0)
    self.music:setVolume(0.2)
    self.music:stop()
    if not MusicMuted then
        self.music:play()
    end
end

function scene:pause()
    self.musicPosition = self.music:tell()
    self.music:pause()
end

function scene:leave()
    self.music:stop()
end

function scene:resume()
    self.music:stop()
    if not MusicMuted then
        self.music:play()
        print ("Music position: " .. self.musicPosition)
        self.music:seek(self.musicPosition)
    end
end

function scene:update(dt)
    LevelManager.currentLevel():update(dt)
    if self.playerMovementCooldownRemaining > 0 then
        self.playerMovementCooldownRemaining = self.playerMovementCooldownRemaining - dt
    end
end

function scene:draw()
    love.graphics.setColor(1, 1, 1) --white

    LevelManager.currentLevel():draw()
end

function scene:handleInput()
    if Input:isPressed("click") then
        if LevelManager.currentLevel().started == false then
            -- start level from intro
            LevelManager.currentLevel():start()
        elseif LevelManager.currentLevel():isWon() then
            if NextLevel() == false then    -- goes to next level if there is one
                Roomy:enter(Scenes.credits) -- show credits screen
            end
        elseif LevelManager.currentLevel():isLost() then
            -- restart level
            LevelManager.currentLevel():reset()
            LevelManager.currentLevel():start()
        else
        -- gameplay stuff
            --sounds.meow:setPitch(0.5+math.random())
            --sounds.meow:play()
        end
    end

    if LevelManager.currentLevel().started
    and not LevelManager.currentLevel():isWon()
    and not LevelManager.currentLevel().preWon
    and not LevelManager.currentLevel():isLost()
    then
        -- movement controls
        local directionKeys = {"up", "down", "left", "right"}
        local directionMapping = {
            up = {x=0, y=-1},
            down = {x=0, y =1},
            left = {x=-1, y=0},
            right = {x=1, y=0},
        }
        for _, dk in ipairs(directionKeys) do
            if Input:isPressed(dk) or Input:isDown(dk) then
                if self.playerMovementCooldownRemaining <= 0 then
                    LevelManager.currentLevel():movePlayer(directionMapping[dk].x, directionMapping[dk].y)
                    LevelManager.currentLevel().player.direction = dk
                    self.playerMovementCooldownRemaining = self.playerMovementCooldownMax
                end
            end
        end
    end

    if Input:isPressed("pause") then
        Roomy:push(Scenes.pause)
    end

    if Input:isPressed("menu") then
        Roomy:push(Scenes.menu)
    end

    -- cheat to win current level
    if Input:isPressed("cheat") then
        LevelManager.currentLevel().won = true
    end

    if Input:isPressed("quit") then
        Roomy:push(Scenes.title)
    end

    if Input:isPressed("mute") then
        MusicMuted = not MusicMuted
        if MusicMuted then
            self.music:pause()
        else
            self.music:play()
        end
    end
end

return scene
