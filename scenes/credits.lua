local scene = {}

function scene:draw()
    love.graphics.setColor(1, 1, 1) --white
    love.graphics.setFont(Fonts.default)
    love.graphics.printf("Thanks for playing! <3", 0, CANVAS_HEIGHT*0.1, CANVAS_WIDTH, "center")

    -- love.graphics.printf("If you enjoyed this game, say hi or check out our other games! :)", CANVAS_WIDTH*0.1, CANVAS_HEIGHT*0.3, CANVAS_WIDTH*0.8, "center")

    -- love.graphics.setColor(0, 0.4, 0.6)
    -- love.graphics.arc("fill", CANVAS_WIDTH*0.3, CANVAS_HEIGHT*0.7, 50, math.pi/6, -math.pi/6+math.pi*2)
    -- love.graphics.print("flauschzelle.de", CANVAS_WIDTH*0.2, CANVAS_HEIGHT*0.5)
    -- love.graphics.setColor(0.8, 0.5, 0)
    -- love.graphics.arc("fill", CANVAS_WIDTH*0.7, CANVAS_HEIGHT*0.7, 50, math.pi/6+math.pi, -math.pi/6+math.pi*3)
    -- love.graphics.print("blinry.org", CANVAS_WIDTH*0.6, CANVAS_HEIGHT*0.5)

    -- love.graphics.setColor(1,1,1)
    -- for i = 1,7 do
    --     love.graphics.circle("fill", CANVAS_WIDTH*(0.3 + 0.05*i), CANVAS_HEIGHT*0.7, 15)
    -- end

    love.graphics.setColor(0.8, 0.8, 0.8)
    love.graphics.printf("(Press "..Input:getKeyString("click").." to go back to the title screen.)", 0, CANVAS_HEIGHT*0.8, CANVAS_WIDTH, "center")
end

function scene:handleInput()
    if Input:isPressed("click") then
        Roomy:enter(Scenes.title)
    end
end

return scene