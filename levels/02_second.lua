local name = "Second Level"
local config = {
    intro = "Welcome to level 2. Can you solve it?",
    preOutro = "Yay, you made it!",
    outro = "But you still have another level to play... And have you found what we say in this hint yet?",
    tileColor = {0, 0.4, 0.6, 1}, -- dark cyan
    obstacles = 0.2,
    width = 12,
    height = 9,
}

local level = Level:new(name, config)

return level