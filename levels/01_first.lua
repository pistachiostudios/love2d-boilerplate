local name = "First Level"
local config = {
    intro = "Pick up all the dots! (Press "..Input:getKeyString("click").." to start)",
    preOutro = "Good job!",
    outro = "But you still have another level to play!",
    obstacles = 0,
}

local level = Level:new(name, config)

return level