local name = "Third Level"
local config = {
    intro = "Welcome to level 3.",
    outro = "This was the last level, you won the game!",
    obstacles = 0.4,
    width = 15,
    height = 15,
}

local level = Level:new(name, config)

return level