# GAME TITLE

...game description...-

Made in 72 hours for Ludum Dare (number) game jam:  
(link to ldjam page)

## Credits (internal)

- Graphics, programming, level design: (names)

## Credits (external)
*Assets from external sources that were used in this game*

Sound effects:

- (insert credits for sound effects from external sources here)

Music:

- (insert credits for music from external sources here)
