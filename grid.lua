-- a class for grids
-- and some functions for managing them

local Grid = Class("Grid")

function Grid:initialize(config)
    self.config = config
    -- size of the playing field (number of tiles)
    self.width = config.width or 9
    self.height = config.height or 3
    self.config.tileColor = config.tileColor or {0.2, 0.2, 0.2, 1.0}
    self.config.wallColor = config.wallColor or {0.5, 0.5, 0.5, 1.0}
    self.tiles = {}
    self.walls = {}
    self.config.obstacles = self.config.obstacles or 0.2

    for x = 1, self.width do
        self.tiles[x] = {}
        for y = 1, self.height do
            self.tiles[x][y] = true
        end
    end

    self:setWalls()
end

function Grid:isWalkable(x, y)
    if x < 1 or x > self.width or y < 1 or y > self.height then
        return false
    elseif self:isWall(x,y) then
        return false
    end
    return self.tiles[x][y]
end

function Grid:getRandomFreePosition()
    local x = math.random(1, self.width)
    local y = math.random(1, self.height)
    while not self:isWalkable(x, y) do
        x = math.random(1, self.width)
        y = math.random(1, self.height)
    end
    return x, y
end

function Grid:isWall(x, y)
    if self.config.obstacles > 0 then
        if self.walls[x][y] ~= false and self.walls[x][y] ~= nil then
            return true
        else
            return false
        end
    else
        return false
    end
end

function Grid:breakWall(x, y)
    if self:isWall(x, y) then
        self.walls[x][y] = self.walls[x][y] - 0.2001
        --sounds.crunch:setPitch(1.2 + math.random() * 0.2)
        --sounds.crunch:play()
    end
    if self.walls[x][y] <= 0 then
        self.walls[x][y] = false
    end
end

function Grid:testFlood(wx,wy)
    -- build test grid
    local floodGrid = {}
    for x = 1, self.width do
        floodGrid[x] = {}
        for y = 1, self.height do
            floodGrid[x][y] = nil
            if self.tiles[x][y] ~= nil then
                if self:isWall(x, y) == false then
                    floodGrid[x][y] = false
                end
            end
        end
    end
    -- set wall tile to be tested:
    floodGrid[wx][wy] = nil

    -- flooding
    local changed = nil
    -- start in top left corner (because there is no wall at the player's start position)
    floodGrid[1][1] = true
    while true do
        changed = 0
        for x = 1, self.width do
            for y = 1, self.height do
                if floodGrid[x][y] == false then
                    if (x < self.width and floodGrid[x+1][y] == true) or
                    (x > 1 and floodGrid[x-1][y] == true) or
                    (y < self.height and floodGrid[x][y+1] == true) or
                    (y > 1 and floodGrid[x][y-1] == true)
                    then
                        floodGrid[x][y] = true
                        changed = changed + 1
                    end
                end
            end
        end
        if changed == 0 then
            -- flooded everything that can be
            break
        end
    end
    -- check if there are unflooded tiles left:
    for x = 1, self.width do
        for y = 1, self.height do
            if floodGrid[x][y] == false then
                return false
            end
        end
    end
    -- if this is reached, everything can still be flooded with a wall tile at wx, wy
    return true

end

function Grid:setWalls()
    -- init walls grid with false:
    for x = 1, self.width do
        self.walls[x] = {}
        for y = 1, self.height do
            self.walls[x][y] = false
        end
    end
    -- set some walls to true, maybe:
    for i = 1, self.width * self.height do
        local x = math.random(1, self.width)
        local y = math.random(1, self.height)
        if (x > 1 or y > 1) and self.tiles[x][y] ~= nil then
            if (math.random() < self.config.obstacles ) and self:testFlood(x, y) then
                self.walls[x][y] = 1
            end
        end
    end
end


function Grid:draw(offsetX, offsetY)

    local rx = TileSize / 8

    -- draw tiles
    love.graphics.setColor(unpack(self.config.tileColor))
    for x = 1, self.width do
        for y = 1, self.height do
            local px = offsetX + (x - 1) * TileSize
            local py = offsetY + (y - 1) * TileSize
            local w = TileSize
            local h = TileSize
            love.graphics.rectangle("fill", px, py, w, h, rx)
        end
    end

    -- draw walls
    if self.config.obstacles > 0 then
        for x = 1, self.width do
            for y = 1, self.height do
                if self.walls[x][y] ~= false then
                    if self.walls[x][y] == 1 then
                        love.graphics.setColor(unpack(self.config.wallColor))
                    elseif self.walls[x][y] < 1 then
                        local v = self.walls[x][y]
                        local r = self.config.wallColor[1] * v
                        local g = self.config.wallColor[2] * v
                        local b = self.config.wallColor[3] * v
                        local a = self.config.wallColor[4] * v
                        love.graphics.setColor(r, g, b, a)
                    end
                    love.graphics.rectangle("fill", offsetX+(TileSize*(x-1)), offsetY+(TileSize*(y-1)), TileSize, TileSize, TileSize/8)
                end
            end
        end
    end
end

return Grid
