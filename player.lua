local Player = Class("Player")

function Player:initialize(x, y, color)
    -- position in grid coordinates
    self.x = x
    self.y = y
    self.direction = "down"
    self.color = color
    self.image = Images.child
    -- init state of animations etc. here
end

function Player:update(dt)
    -- animation of the player character (like
    -- mouth movement, walking, powerups, etc.) goes here
end

function Player:draw(offsetX, offsetY, opacity)

    -- grid-based position
    local posX = offsetX + (self.x - 0.5) * TileSize
    local posY = offsetY + (self.y - 0.5) * TileSize

    local rotation = 0

    if self.direction == "right" then
        -- look to the right
        rotation = math.pi * 1.5
    elseif self.direction == "down" then
        --look down
        rotation = 0
    elseif self.direction == "left" then
        -- look left
        rotation = math.pi * 0.5
    elseif self.direction == "up" then
        -- look up
        rotation = math.pi
    end

    love.graphics.setColor(self.color[1], self.color[2], self.color[3], opacity)

    local width = self.image:getWidth()
    local height = self.image:getHeight()

    love.graphics.draw(self.image, posX, posY, rotation, 1, 1, 0.5 * width, 0.5 * height)

end

return Player
