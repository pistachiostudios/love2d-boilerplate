-- a class for levels
-- and some functions for managing them

Grid = require "grid"
Player = require "player"

local Level = Class("Level")

function Level:initialize(name, config)

    self.name = name
    self.config = config
    self.contentText = self.config.contentText

    self.started = false
    self.won = false
    self.lost = false

    -- nil is acceptable as a default value here!
    self.intro = self.config.intro
    -- start immediately if the level has no intro
    if self.intro == nil then
        self:start()
    end

    -- preOutro text, may be nil
    self.preOutro = self.config.preOutro

    -- default outro text
    self.outro = self.config.outro
    if self.outro == nil then
        self.outro = "Congratulations, you won! Click to continue."
    end

    -- default lost text
    self.lostMessage = self.config.lostMessage
    if self.lostMessage == nil then
        self.lostMessage = "Oh no! Press "..Input:getKeyString("click").." to restart."
    end

    -- init player color
    if config.playerColor == nil then
        self.config.playerColor = {1, 1, 1, 1} -- default: opaque white
    else
        self.config.playerColor = config.playerColor
    end

    -- default grid size
    self.config.width = self.config.width or 9
    self.config.height = self.config.height or 3

    local gameWidthInTiles = self.config.width
    local gameHeightInTiles = self.config.height

    self.TileSize = math.min(
        (CANVAS_WIDTH - (2 * MarginSize))/gameWidthInTiles,
        (CANVAS_HEIGHT - (2 * MarginSize))/gameHeightInTiles
    )

    -- init grid
    self.grid = Grid:new(self.config)

    -- init empty grid of collectible objects
    self.objects = {}
    for x = 1, self.grid.width do
        self.objects[x] = {}
        for y = 1, self.grid.height do
            self.objects[x][y] = false
        end
    end

    self.preWonTime = 2.5 -- duration between first and second outro text
    if self.preOutro == nil then
        self.preWonTime = 0
    end
    self.preWonTimer = self.preWonTime

    self.moveDiscarder = 0 -- this is used for stopping in front of walls

    self:reset()

end

function Level:placeObject(x, y)
    if self.grid:isWall(x, y) then
        self.grid.walls[x][y] = false -- remove wall if there is one
    end
    self.objects[x][y] = true
end

function Level:countObjects()
    local object_count = 0
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.objects[x][y] == true then
                object_count = object_count + 1
            end
        end
    end
    return object_count
end

function Level:update(dt)
    if not(self.lost or self.won or self.preWon or not self.started) then
        -- put level-specific gameplay logic here

        -- update player
        self.player:update(dt)

        -- try to pick up object
        self:pickupObject(self.player.x, self.player.y)

    elseif self.preWon then
        self.preWonTimer = self.preWonTimer - dt
        if self.preWonTimer <= 0 then
            self.won = true
        end
    end
end

function Level:movePlayer(dx, dy)
    -- Check if the player can move away from the current position.
    if not self.grid:isWalkable(self.player.x, self.player.y) then
        return
    end

    -- If target position is walkable, move the player there.
    local newX = self.player.x+dx
    local newY = self.player.y+dy
    if self.grid:isWalkable(newX, newY) then
        self.player.x = newX
        self.player.y = newY
    else
        if newX <= self.config.width and newX > 0 and newY <= self.config.height and newY > 0 then
            if self.grid:isWall(newX,newY) then
                if self.moveDiscarder > 0 then
                    self.moveDiscarder = self.moveDiscarder - 1
                else
                    -- damage wall when running against it
                    self.grid:breakWall(newX, newY)
                    self.moveDiscarder = 1
                end
            end
        end
    end
end

function Level:pickupObject(x, y)
    if self.objects[x][y] == true then
        Sounds.meow:setPitch(0.5+math.random())
        Sounds.meow:play()

        -- remove object from the map and counter
        self.objects[x][y] = false

        self.remaining_objects = self.remaining_objects -1
        -- check win condition
        if self.remaining_objects == 0 then
            self:win()
        end
    end
end

function Level:draw()

    -- draw the actual level content here:

    -- first draw background stuff:
    TileSize = self.TileSize
    local gridOffsetX = MarginSize
    local gridOffsetY = MarginSize

    self.grid:draw(gridOffsetX, gridOffsetY)

    -- the 'contentText' is just a placeholder for actual game content
    if self.contentText then
        love.graphics.setFont(Fonts.default)
        love.graphics.setColor(1, 0.5, 0)
        love.graphics.printf(self.contentText, 0, CANVAS_HEIGHT*0.75, CANVAS_WIDTH, "center")
    end

    -- draw objects
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.objects[x][y] == true then
                love.graphics.setColor(0.5, 0.5, 1, 1)
                love.graphics.circle(
                    "fill",
                    gridOffsetX+(TileSize*(x-0.5)),
                    gridOffsetY+(TileSize*(y-0.5)),
                    TileSize * 0.2
                )
            end
        end
    end

    -- then draw the player:

    if not self.lost then
        self.player:draw(gridOffsetX, gridOffsetY, 1)
    else
        -- player gets transparent when lost
        self.player:draw(gridOffsetX, gridOffsetY, 0.3)
    end

    -- then draw foreground objects and other stuff that might be on top of the player:

    -- at last, draw text overlays for some special states:

    love.graphics.setFont(Fonts.default)
    love.graphics.setColor(1,1,1)
    if self.preWon == true and self.won == false then -- first step of outro
        DimRect()
        if self.preOutro ~= nil then
            love.graphics.printf(self.preOutro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
        end
    elseif self.won == true then -- second step of outro
        DimRect()
        if self.preOutro ~= nil then
            love.graphics.printf(self.preOutro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
        end
        love.graphics.printf(self.outro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/2, CANVAS_WIDTH * 0.8, "center")
    elseif self.lost == true then -- lost message
        DimRect()
        love.graphics.printf(self.lostMessage, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/2, CANVAS_WIDTH * 0.8, "center")
    elseif (self.started == false and self.intro ~= nil) then -- intro
        DimRect()
        love.graphics.printf(self.intro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
    else
        -- any other conditions?
    end

end

function Level:start()
    self.started = true
end

function Level:isWon()
    return self.won
end

function Level:isLost()
    return self.lost
end

function Level:win()
    if not self.preWon then
        self.preWon = true
        -- put logic for the start of the outro here
        -- (e.g. play a sound)
    end
end

function Level:reset()

    self.grid:setWalls() -- reset walls
    self.player = Player:new(1, 1, self.config.playerColor)

    -- place one object as a default:
    local objX = self.grid.width
    local objY = self.grid.height
    self:placeObject(objX, objY)

    self.remaining_objects = self:countObjects()
    
    self.preWon = false
    self.won = false
    self.lost = false
    self.preWonTimer = self.preWonTime

    -- not setting 'started' to false here because this is also
    -- used for restarting and the intro should not be shown again then
end

-- go to next level if there is one, return false if not:
function NextLevel()
    if LevelManager.current < LevelManager.levelCount then
        -- got to next level
        LevelManager.current = LevelManager.current + 1
        -- reset previous level to not won (and not started)
        Levels[LevelManager.current - 1]:reset()
        if Levels[LevelManager.current - 1].intro ~= nil then
            Levels[LevelManager.current - 1].started = false
        end

        return true
    else
        return false
    end
end

-- initialize levels list:
Levels = {}

-- this must be called once after the levels have been loaded from files:
function InitLevelManager()
    LevelManager = {
        current = 1,
        levelCount = #Levels,
        currentLevel = function ()
            return Levels[LevelManager.current]
        end,
    }
end

return Level